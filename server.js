const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const dotenv = require('dotenv');
dotenv.config();

const mongoURI = process.env.MONGO_URI || 'mongodb://localhost:27017/';
mongoose
  .connect("mongodb+srv://kushaldwivedi1:1234@cluster0.svcfjgq.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0")
  .then(() => console.log('MongoDB connected successfully'))
  .catch(err => console.error(err));

const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use(cors());
app.options('*', cors());

const feedbackData = [];

const surveys = [
  {
    "id": 1,
    "title": "New student survey",
    "desc": "Questions for first year students",
    "nq": 2,
    "qs": ["3", "4"]
  },
  {
    "id": 2,
    "title": "Student survey",
    "desc": "Questions for master program students",
    "nq": 4,
    "qs": ["1", "2", "3", "4"]
  }
];

const questions = [
  {
    "id": 1,
    "type": "rate",
    "title": "Were you satisfied with the organization of the course?",
    "description": "On the scale between 1 (lowest) and 5 (highest) please rate organization of the course",
    "options": ["1", "2", "3", "4", "5"]
  },
  {
    "id": 2,
    "type": "rate",
    "title": "Were you satisfied with the content you got at the course?",
    "description": "On the scale between 1 (lowest) and 5 (highest) please rate content of the course",
    "options": ["1", "2", "3", "4", "5"]
  },
  {
    "id": 3,
    "type": "rate",
    "title": "Rate our education center",
    "description": "On the scale between 1 (lowest) and 5 (highest) please rate our education center",
    "options": ["1 - Poor", "2 - Average", "3 - Good", "4 - Very Good", "5 - Excellent"]
  },
  {
    "id": 4,
    "type": "free",
    "title": "Why did you select this course?",
    "description": "Please describe the reasons why you selected the course",
    "options": []
  },
]

const filters = {
  "Student": [3],
  "Survey": [1, 2, 3, 4]
};

const ResponseModel = mongoose.model('Response', {
  name: String,
  email: String,
  educationPhase: String,
  comments: String,
});

app.post('/submitFeedback', (req, res) => {
  const newFeedback = req.body;
  save(newFeedback);
  res.json({ success: true, message: 'Feedback submitted successfully!', feedbacks: feedbackData });
});

app.get('/feedbacks', (req, res) => {
  res.json(feedbackData);
});

app.get('/surveys', (req, res) => {
  res.json(surveys.map(survey => {
    const surveyQuestions = survey.qs.map(questionId => {
      const question = questions[educationPhase].find(q => q.id === parseInt(questionId));
      if (question) {
        return question;
      } else {
        console.error(`Question with ID ${questionId} not found for survey ${survey.id}`);
        return null;
      }
    }).filter(question => question !== null);
    return { ...survey, questions: surveyQuestions };
  }));
});

app.get('/questions/:educationPhase', (req, res) => {
  const educationPhase = req.params.educationPhase;
  if (filters[educationPhase]) {
    res.json(questions.filter(question => filters[educationPhase].includes(question.id)));
  } else {
    res.status(404).json({ error: 'Questions not found for the specified education phase' });
  }
});

app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, 'public', 'mypage.html'));
});

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});

function save(data) {
  const newDocument = new ResponseModel(data);
  newDocument.save().then(
    data => console.log(data)
  ).catch(err => {
    console.log(err);
  });
}